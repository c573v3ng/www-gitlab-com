---
layout: handbook-page-toc
title: Spam
category: GitLab.com
subcategory: Managing Spam
description: "Trust and Safety workflow(s) for Managing spam reported on Gitlab.com"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Spammers use GitLab.com to distribute various forms of Spam.  Starting with 8.11, GitLab has better integration with Akismet to check for spam whenever a user creates an issue. 
This filter needs to be trained properly; with some effort our spam detection continues to improve. 

# Training the spam filter

If an issue is created on GitLab.com by a spammer, before deleting the account, flag the user's issues as spam:

* Login to GitLab.com with an admin account

* View each issue and click on "Submit as spam"

# Removing bulk Spam

1.  Bulk spam should be speedily remediated.  Follow the instructions for Bulk Spam Removal  [SPAM](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/wikis/Runbooks/Cleanup_Massive_Spam)

# Report of Spam (Customer)

If we receive reports from customer accounts on Gitlab.com being spammed, follow the workflow below. 

1.  Confirm that the content is still live and available. 
    1.  If the content is no longer available/removed, close the report.
    1.  Add an admin note to the account. 

1.  If the content is still available, continue to disable/remove the content. 
    1.  Place a block on the reported account. 
    1.  Add an admin note to the account. 
    1.  Notify the reporter

