
---
layout: handbook-page-toc
title: Blocked Accounts
category: GitLab.com
subcategory: Reinstating Blocked Accounts
description: "Trust and Safety workflow for Blocked Account requests"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Accounts can be blocked for a variety of reasons.  As we have multiple systems monitoring for unusual activity which violate our terms, some of the alerts might result in accounts automatically being blocked.  
This workflow will stipulate the steps required for requesting a review of an account blocked by the Trust and Safety Team.

# Support Workflow

1.  For block removal requests that arrive via the support team can follow the [Support reinstating-blocked-accounts](https://about.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts.html) workflow. 

