---
layout: handbook-page-toc
title: PII / Sensitive Information
category: GitLab.com
subcategory: Removal of PII or Sensitive information
description: "Trust and Safety workflow for PII removal"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

PII) is any data that could potentially identify a specific individual. Any information that can be used to distinguish one person from another. PII can be sensitive or non-sensitive. Non-sensitive PII is information from public records, phone books, corporate directories and websites without resulting in harm to the individual. Sensitive PII is information which, when disclosed, could result in harm to the individual whose privacy has been breached. This can be information publicly accessible via a GitLab.com

Keywords: PII, Name, Social Security Number, Date and Place of birth, Mother's Maiden Name, Biometric Record, Résumé, Education Information etc.

PII requests have a 48 hour SLA

Workflow

 Workflow

## First 24 hours

1. Vet the request.
1. Verify that the reported content links to the content is present in the request.
1. If the content is no longer available. Notify the requester and close the request.
1. If the report is accurate and the content is still live, continue to `Takedown` request. 

 `PII::Notice to content 'Owner'::First Touch` macro.  Including the reference ticket in Internal Notes.

## Taking down the content 

1. If the content violates our [Terms of Service](https://about.gitlab.com/terms/), continue to disable the reported content. 
1. Reach out to the account owner using the `PII::Notice to content 'Owner'::First Touch` macro. 
1. Add an internal note to the user account and pend the ticket for 24h. 
1. Notify the Reporter that the content has been disabled. 

# Next 24 hours

1. If the account owner responded, review the provided information with Legal (if required). 
1. If we receive no response from the requester, proceed to reply using the `PII::Notice to content 'Owner'::Second Touch` macro.
1. Pend the ticket for a further 24 hours. 

## Final Touch

1. If the account owner has not responded, continue to disable the account. 
1. Send a follow-up notice to the user informing them that action has been taken, using the macro `PII::Notice to content 'Owner'::(take down)` macro.
1. Add an internal note to the account of the actions taken on the content and resolve the ticket.

## User disputes a takedown notice

### Should the user dispute the takedown request, follow the below steps:
{:.no_toc}

1. If the user disputes the notice; continue to engage with Legal on next steps. 
1. Add an internal note referencing the Legal request and pend for 24 hours.
1. After the 24 hours, continue to engage with Legal/Reporter to resolve the issue. 
1. Once resolved, close all tickets and issues.  Leave an internal note on the account referencing the relevant issue.  

