---
layout: handbook-page-toc
title: Macros
category: Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 

We use Zendesk Macros as a tool to reach out to our customers and to maintain consistency in our communication.
It's important to find a balance of when and where to use a macro.  We strive to not sound too robotic and add a human factor to our 
responses.

## Who can create a Macro?

Macros can be created by engineers and managers in Zendesk. They should
be used for matters that require a rigid process (DMCA/Unblock Requests/Policy Violations) or when we 
_need_ to provide a template response (Acknowledgments). If you need to create
a macro that add tags to tickets, make sure to get approval the Support
Operations team beforehand.

## How do I create a Macro?

Macros are version controlled and implemented via CI/CD using the
[zendesk-macros](https://gitlab.com/gitlab-com/support/support-ops/zendesk-macros)
project. To create a macro, you would create a new JSON file in the
[macros folder](https://gitlab.com/gitlab-com/support/support-ops/zendesk-macros/-/tree/master/macros).
When you make the commit, it will have you create a branch to apply it. From
there, you will use a Merge Request and get approval. Once merged, CI/CD will
handle any updates/creations needed on the Zendesk side.

If you are unsure of the syntax or wording to use for the JSON file, feel free
to
[create an issue](https://gitlab.com/gitlab-com/support/support-ops/zendesk-macros/issues/new)
in the project. Assign it to a member of Support Operations to get assistance in
generating the file.

## Where Trust and Safety Macros Live

Macros are name spaced with a `::` which nests them in the bottom of Zendesk
Dropdown. The current top-level headings are below:

* DMCA - Responses for the DMCA process.
* Security - General Response process. 

