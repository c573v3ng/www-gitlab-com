---
layout: handbook-page-toc
title: Reports
category: Abuse Reports
subcategory: Abuse Reports Workflows - Gitlab.com
Description:  "Abuse Reports Trust and Safety"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 

Users and GitLab team members can report violations of the `GitLab.com` [Terms of Use](https://about.gitlab.com/terms/#gitlab-com) to the `Trust & Safety` team via the [`Report Abuse`](https://docs.gitlab.com/ee/user/abuse_reports.html) button found on the user profile in the `GitLab.com` UI. Reports can also be submitted for comments, issues, merge requests, snippets, using the `report abuse to admin` button.  

Users who do not have a `GitLab.com` account, can submit an abuse report via email to `abuse@gitlab.com`. The report should contain a link to the implicated content as well as the reason you believe the content is in violation of our Terms of Use. R

## Identifying Trust and Safety related issues

* Blocked accounts, see [Reinstating Blocked Accounts](https://about.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts.html) workflow for more information. 
* * Reports of phishing pages/repositories/Groups 
    * For suspected phishing emails sent to GitLab team members, please read the (Security Handbook Page)[https://about.gitlab.com/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack] page for more information. 

* Malware/Malicious content
* Spam on Gitlab.com 
* Reports from industry reporters/partners. 
* DMCA Takedown Requests
* Sign up restrictions (add/remove) 


## Issues that are outside the scope of the Trust and Safety. 

* CloudFlare, HAProxy and other IP Blocks/Removals 
* Log information requests - see [Log and audit requests](/handbook/support/workflows/log_requests.html)
* Taxing Gitlab Resources - Engaging with [Infra](https://gitlab.com/gitlab-com/runbooks#general-guidelines-for-production-incidents) | [SIRT](https://gitlab.com/gitlab-com/gl-security/runbooks/-/tree/master/sirt#general-and-oncall)
* Compromised Accounts - [SIRT](https://about.gitlab.com/handbook/engineering/security/#engaging-the-security-on-call) 

## Workflows 

# Security Testing 

We have customers who participate in our @wearehackerone program. We often receive reports of the accounts, testing is permitted on gitlab.com as long as the guidelines set out in the [Gitlab Bug Bounty Program](https://hackerone.com/gitlab?type=team) are adhered to and the activity is not disruptive to our community.  

# Malware 

Reports of malware should be investigated and removed promptly if the investigation concludes that the content is malicious. In the event that we need more information regarding the activity of the file(s), follow the instructions set out at [Investigating Malware Reports](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/wikis/Abuse_Type/Malware)

# SPAM 

We often receive reports of spam.  Spam is not only large volume but can often contain URL(s) to untrusted websites which might compromise your device. For more information see [SPAM](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/wikis/Runbooks/Spam_Gitlab.com) for more information. 

# Sensitive and Personal Identifiable Information Removal (PII)

When we receive reports of sensitive or PII on Gitlab.com, we should act quickly in actioning the reports once verified. See [PII Removal](https://about.gitlab.com/handbook/support/workflows/pii_removal_requests.html)

