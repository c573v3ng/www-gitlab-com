---
layout: handbook-page-toc
title: Trust and Safety Quick Reference
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tools Used by Request Type

The following table contains links to the various tools you may need to use in order to follow the workflows above.


| Request               |  |
| ------- |:------:|:------:|:------:|:------:|:------:|
| 2FA | X | X | X |   |   |
|  |   | X | X |   | X |
|  |   |   |   | X |   |
|  |   |   |   | X |   |
|  |   |    |   | X |   |
| |   |    |   | X |   |
|  |   | X | X |   |   |
|  |   |   |   |   |   |
|                     |   |   |   |   |   |

#### Tools Tips
* [API Username Lookup](https://gitlab.com/api/v4/users?search=emal@email.com): Use in Firefox for an easier to read, parsed output
* [CustomersDot - Admin](https://customers.gitlab.com/admin/): Shared login, password in vault.


