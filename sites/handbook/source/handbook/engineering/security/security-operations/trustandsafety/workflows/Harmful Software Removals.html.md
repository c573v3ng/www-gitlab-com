---
layout: handbook-page-toc
title: Harmful Software Removals (Malware)
category: Reports
subcategory: Malware Removal Requests
Description:  "Workflow for the removal of harmful files"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This workflow focuses on the process for the removal of harmful content reported to the Trust and Safety Team. 

# Reports - Trusted Reporters 

- 

# Reports - Zendesk

Reports of Malware or harmful software should be removed as soon as the report has been reviewed.  
If there are any problems with actioning the takedown of the content, reach out to the team for guidance or assistance. 

# Workflow 

1.  Confirm that the content is still live and available. 
    1. if the content is no longer available, send the `Content no longer available` blurb and close the ticket.
    1. Add an admin note to the reported account.
1.  If the content is still available
    1. Open up a new [Malware Report](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/issues/new).
    1. Review the information provided in the report and follow the workflow instructions in the issue. 
    1. Take action on the reported content should it be abusive. 

1.  Account owner requests more information about blocked account. 

 * Account review requests are sent through to support@gitlab.com. The DRI for the Ticket will follow the [Reinstating Blocked Accounts](https://about.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts.html) workflow. 

    1.  Review the `Account Review Request` issue opened by the Support Team member. 
 
1.  Next 24 Hours (Zendesk Outbound created)

1. Confirm that the account owner has responded.
   1.  If no response, follow up with the issue assignee on doing a follow up.
   1.  Pend the ticket for 24 hours.
   1. If the account owner responded, continue to engage with the Trust and Safety Team to review the information provided.  



