---
layout: handbook-page-toc
title: Security Testing
category: GitLab.com
subcategory: Security Testing
description: "Security Testing on Gitlab.com"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Some of our users also participate in our @wearehackerone program. 
Participants of the program are encouraged to perform testing on their own GitLab instances, however we do allow very limited testing on `GitLab.com`. If a users activity is deemed disruptive, mitigating action will be taken on the account. Please refer to the `Rules of Engagement, Testing, and Proof-of-concepts` section on the [Gitlab Bug Bounty Program](https://hackerone.com/gitlab?type=team) site for more details.  

# Reviewing Accounts


# Policy Violation

  1.  Review the reported account. 
      1. If the account does not meet the guidelines for our `Gitlab Bug Bounty Program`, continue to review the account as per `ToS Violation` workflow. 
      1. If the account does meet the requirements, continue to review the account as per `ToS Violation` workflow.
      1. Resolve the report leaving an admin note on the account to the report. 

# Remediation 

  1.  Sometimes customers can cause unintentional abuse. In the event that the account owner identifies the abuse as uninentional, continue to follow the 
      `ToS Violation` workflow to determine remediation of the reported activity. 

