---
layout: handbook-page-toc
title: Support Operations GitLab Processes
description: List of Support Operations GitLab Processes which includes Time Tracking, Triage, Priority, Stage and Category.
---

# GitLab Processes

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Time Tracking

Support-Ops utilizes 
[Time Tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html)
in the issues/MRs we work. We utilize this time tracking to help define our
hiring model, measure workload, and etc. As you work issues and merge requests,
make sure you utilize this feature. 

## Triage

At least once a day, Support-Ops should triage the
[issues](https://gitlab.com/groups/gitlab-com/support/support-ops/-/issues) and
[MRs](https://gitlab.com/groups/gitlab-com/support/support-ops/-/merge_requests)
that appear under the
[support-ops group](https://gitlab.com/gitlab-com/support/support-ops).

Support-Ops also makes use of the
[GitLab Triage Gem](https://gitlab.com/gitlab-org/gitlab-triage) to assist in
ensuring all issues/merge requests are triaged. 

## Labels

For a list of the labels we use and what they mean, please see the [Support Ops Project README](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project#what-do-all-the-labels-mean)
