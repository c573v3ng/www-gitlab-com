---
layout: handbook-page-toc
title: Virtual Workshops
description: Virtual Workshops at GitLab, including processes to launch and responsibilities by team.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Virtual Workshops Overview
{:.no_toc}
---
