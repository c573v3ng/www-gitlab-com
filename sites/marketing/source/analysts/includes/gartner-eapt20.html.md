---
layout: markdown_page
title: "Gartner Magic Quadrant for Enterprise Agile Planning Tools (EAPT) 2020"
description: "This page shows how Gartner views our enterprise agile planning tools capabilities in relation to the larger market in 2020."
canonical_path: "/analysts/gartner-eapt20/"
---
## GitLab and the Gartner Magic Quadrant for Enterprise Agile Planning Tools (EAPT) 2020*
This page represents how Gartner views our enterprise agile planning tools capabilities in relation to the larger market and how we're working with that information to build a better product. It also provides Gartner with ongoing context for how our product is developing in relation to how they see the market.

![Gartner EAPT MQ 2020](/images/analysts/gartner-magic-quadrant.jpg){: .small}

This graphic was published by Gartner, Inc. as part of a larger research document and should be evaluated in the context of the entire document. The Gartner document is available upon request by clicking the link on this page.
{: .note .font-small}

### Gartner's Key Takeaways on the EAPT Market at the time of publication:

**Market Definition/Description**
"Agile is the dominant means of creating software today because it enables organizations to respond to change quickly, to learn rapidly, and to deliver continuously. Making use of agile practices at scale is essential to digital business success."



### Gartner's observations about GitLab

tbd

### The GitLab Enterprise Agile Planning vision

tbd


Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s research organization and should not be construed as statements of fact. Gartner disclaims all warranties, express or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.
{: .note .font-small .margin-top40}

Gartner, Magic Quadrant for Enterprise Agile Planning Tools, 21 April 2020, Keith Mann, Mike West, Bill Blosen, Akis Sklavounakis, Deacon D.K Wan
{: .note .font-small .margin-top40}
